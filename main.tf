provider "aws" {
    region      = "us-east-1"
}

variable "subnet1_cidr_block" {
    description = "subnet_cidr_block"
}

variable "vpc_cidr_block" {
    description = "vpc_cidr_block"
}

variable "environment" {
    description = "deploy dev"
}

resource "aws_vpc" "development-vpc" {
    cidr_block  = var.vpc_cidr_block
    tags        = {
        Name    = var.environment,
        vpc_env = "dev"
    }
}


resource "aws_subnet" "dev-subnet-1" {
    vpc_id            = aws_vpc.development-vpc.id 
    cidr_block        = var.subnet1_cidr_block
    availability_zone = "us-east-1a"
    tags        = {
        Name    = "subnet-1-dev"
    }
}

data "aws_vpc" "existing_vpc" {
    default = true
}

resource "aws_subnet" "dev-subnet-2" { 
    vpc_id            = data.aws_vpc.existing_vpc.id
    cidr_block        = "172.31.0.0/20"
    availability_zone = "us-east-1a"
    tags        = {
        Name    = "subnet-2-dev"
    }
}   